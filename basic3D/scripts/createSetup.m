
if(~isdeployed)
  cd([fileparts(which(mfilename)),'/..']);
end


%%
cla;
fid = figure('Visible', 'off');


%% create system

setup               = [];
setup.info.name     = 'basic3D';

setup.dim       = 3;

setup.roi.x     = [-.05, .05];
setup.roi.y     = [-.05, .05];
setup.roi.z     = [-.05, .05];

sensorsDistance = 0.015;
sensorsArray   	= [];
sensorsArray	= [sensorsArray, createEntityArray([setup.roi.x(1)-sensorsDistance,-.045,-.045], [setup.roi.x(1)-sensorsDistance, .045, .045], [ 1, 0, 0], [1 5 5])];
sensorsArray	= [sensorsArray, createEntityArray([setup.roi.x(2)+sensorsDistance,-.045,-.045], [setup.roi.x(2)+sensorsDistance, .045, .045], [-1, 0, 0], [1 5 5])];
sensorsArray	= [sensorsArray, createEntityArray([-.045,setup.roi.y(1)-sensorsDistance,-.045], [ .045,setup.roi.y(1)-sensorsDistance, .045], [0,  1, 0], [5 1 5])];
sensorsArray    = [sensorsArray, createEntityArray([-.045,setup.roi.y(2)+sensorsDistance,-.045], [ .045,setup.roi.y(2)+sensorsDistance, .045], [0, -1, 0], [5 1 5])];
sensorsArray    = [sensorsArray, createEntityArray([-.045,-.045,setup.roi.y(1)-sensorsDistance], [ .045, .045,setup.roi.y(1)-sensorsDistance], [0, 0,  1], [5 5 1])];
sensorsArray    = [sensorsArray, createEntityArray([-.045,-.045,setup.roi.y(2)+sensorsDistance], [ .045, .045,setup.roi.y(2)+sensorsDistance], [0, 0, -1], [5 5 1])];

% array coils
coilsDistance   = 0.005;
coilsArray   	= [];
for i = 1:5
    coilsArray	= [coilsArray, createEntityArray([setup.roi.x(1)-i*coilsDistance,-.035,-.035], [setup.roi.x(1)-i*coilsDistance, .035, .035], [ 1, 0, 0], [1 3 3])];
    coilsArray	= [coilsArray, createEntityArray([setup.roi.x(2)+i*coilsDistance,-.035,-.035], [setup.roi.x(2)+i*coilsDistance, .035, .035], [-1, 0, 0], [1 3 3])];
    coilsArray	= [coilsArray, createEntityArray([-.035,setup.roi.y(1)-i*coilsDistance,-.035], [ .035,setup.roi.y(1)-i*coilsDistance, .035], [0,  1, 0], [3 1 3])];
    coilsArray 	= [coilsArray, createEntityArray([-.035,setup.roi.y(2)+i*coilsDistance,-.035], [ .035,setup.roi.y(2)+i*coilsDistance, .035], [0, -1, 0], [3 1 3])];
    coilsArray 	= [coilsArray, createEntityArray([-.035,-.035,setup.roi.y(1)-i*coilsDistance], [ .035, .035,setup.roi.y(1)-i*coilsDistance], [0, 0,  1], [3 3 1])];
    coilsArray 	= [coilsArray, createEntityArray([-.035,-.035,setup.roi.y(2)+i*coilsDistance], [ .035, .035,setup.roi.y(2)+i*coilsDistance], [0, 0, -1], [3 3 1])];
end

% coil sphere
coilsSphereRadius 	= 0.09;
sensorsSphereRadius = 0.1;
sphereN             = 100;

nCount  = 0;
a       = 4*pi / sphereN;
d       = sqrt(a);
MTheta  = round(pi/d);
dTheta  = pi/MTheta;
dPhi    = a/dTheta;

coilsSpherePosition     = zeros(sphereN, 3);
sensorsSpherePosition   = zeros(sphereN, 3);

for m = 0:MTheta-1
    theta   = pi*(m+0.5) / MTheta;
    MPhi    = round(2*pi*sin(theta)/dPhi);
    for n = 0:MPhi-1
        phi = 2*pi*n / MPhi;
        coilsSpherePosition(nCount+1,:)     = coilsSphereRadius.*[sin(theta)*cos(phi), sin(theta)*sin(phi), cos(theta)];
        sensorsSpherePosition(nCount+1,:)   = sensorsSphereRadius.*[sin(theta)*cos(phi), sin(theta)*sin(phi), cos(theta)];
        nCount = nCount+1;
    end
end
coilsSpherePosition(nCount+1:end, :) = [];
sensorsSpherePosition(nCount+1:end, :) = [];

coilsSphere = [];
sensorsSphere = [];
for i = 1:nCount
    coilsSphere(i).Position     =  coilsSpherePosition(i,:);
    coilsSphere(i).Normal       = -coilsSpherePosition(i,:);
    sensorsSphere(i).Position	= -sensorsSpherePosition(i,:);
    sensorsSphere(i).Normal     =  sensorsSpherePosition(i,:);
end

% coil    | desc
% -----------------------
% 163:165 | top ring (3)
% 166:174 | 2nd ring (9)
% 175:187 | 3rd ring (13)
% 188:203 | 4th ring (16)
% 204:220 | 5th ring (17)
% 221:236 | 6th ring (16)
% 237:249 | 7th ring (13)
% 250:258 | 8th ring (9)
% 259:261 | 9th ring (3)
% 

setup.coils     =  [coilsArray, coilsSphere];
setup.sensors   =  [sensorsArray, sensorsSphere];

setup.coilGroups    = {...
      0+(1:9),   0+(10:18),   0+(19:27),   0+(28:36),   0+(37:45),   0+(46:54), ...
     54+(1:9),  54+(10:18),  54+(19:27),  54+(28:36),  54+(37:45),  54+(46:54), ...
    108+(1:9), 108+(10:18), 108+(19:27), 108+(28:36), 108+(37:45), 108+(46:54), ...
    162+(1:9), 162+(10:18), 162+(19:27), 162+(28:36), 162+(37:45), 162+(46:54), ...
    216+(1:9), 216+(10:18), 216+(19:27), 216+(28:36), 216+(37:45), 216+(46:54), ...
    271:369};
setup.sensorGroups  = {1:25, 26:50, 51:75, 76:100, 101:125, 126:150, 151:249};


%% save setup variants

setup.coils = setCoilLoopFactor(setup.coils, 1);
setup.info.variant          = 'idealizedUnscaled';
saveSetup(setup.info.variant, setup, 'ff', true);
cla;visualizeMRX(setup, 'fid', fid, 'export', setup.info.variant);
setup.info.variant          = 'idealized';
saveSetup(setup.info.variant, setup, 'ff', true);
cla;visualizeMRX(setup, 'fid', fid, 'export', setup.info.variant);

coil_r0_015_w12_s1000_loop  = createCoilLoop(.015,12,1000,'loop');
coil_r0_015_w12_s1000_loopF = calibrateCoil(coil_r0_015_w12_s1000_loop);
fpop('factor [coil_r0_015_w12_s1000_loop]:',sprintf('%g', coil_r0_015_w12_s1000_loopF));

setup.coils             = setCoilLoopFactor(setup.coils, 1);
setup.coils             = parseCoils(setup.coils,coil_r0_015_w12_s1000_loop);
setup.info.variant      = 'coil_r0.015_w12_s1000_loop';
saveSetup(setup.info.variant, setup, 'ff', true);
cla;visualizeMRX(setup, 'fid', fid, 'export', setup.info.variant);

setup.coils             = setCoilLoopFactor(setup.coils, coil_r0_015_w12_s1000_loopF);
setup.coils             = parseCoils(setup.coils,[]);
setup.info.variant      = 'coil_r0.015_w12_s1000_loop_idealized';
saveSetup(setup.info.variant, setup, 'ff', true);
cla;visualizeMRX(setup, 'fid', fid, 'export', setup.info.variant);

setup.info.variant          = 'default';
saveSetup(setup.info.variant, setup, 'ff', true);
cla;visualizeMRX(setup, 'fid', fid, 'export', setup.info.variant);



%%
close(fid);

