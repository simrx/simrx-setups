
if(~isdeployed)
  cd([fileparts(which(mfilename)),'/..']);
end


%% load setup
setup = loadSetup('default');
fid = figure('Visible', 'off');


%%
config                      = [];
config.info.name            = 'rotation';
config.coilsActive          = unique([setup.coilGroups{:}]);
config.sensorsActive        = unique([setup.sensorGroups{:}]);

config.info.variant         = 'singleSequential';
config.currentPattern       = [];
config.measurementPattern   = [];
for i = 1:numel(setup.coilGroups)
    currentPatternTemp          = createPattern(config.coilsActive, 'sequential', 'entityList', setup.coilGroups{i}, 'current', 1);
    measurementPatternTemp      = createPattern(config.sensorsActive, 'simultaneous', 'entityList', setup.sensorGroups{i}, 'times', size(currentPatternTemp, 1));
    config.currentPattern       = [config.currentPattern; currentPatternTemp];
    config.measurementPattern   = [config.measurementPattern; measurementPatternTemp];
end
if checkCompatibility(setup, config) % check compatibility with setup and save config
    saveConfig(sprintf('configs/%s/%s', config.info.name, config.info.variant), config, 'ff', true);
end

cla;visualizeMRX(setup, 'config', config, 'fid', fid, 'showCoils', 1, 'showSensors', 1, 'export', sprintf('configs/%s/visFull', config.info.name));
cla;visualizeMRX(setup, 'config', config, 'fid', fid, 'showCoils', 1, 'showSensors', 0, 'export', sprintf('configs/%s/visCoils', config.info.name));
cla;visualizeMRX(setup, 'config', config, 'fid', fid, 'showCoils', 0, 'showSensors', 1, 'export', sprintf('configs/%s/visSensors', config.info.name));



%%
config                      = [];
config.info.name            = 'rotationSimple';
config.coilsActive          = unique([setup.coilGroups{1:2:end}]);
config.sensorsActive        = unique([setup.sensorGroups{1:2:end}]);

config.info.variant         = 'singleSequential';
config.currentPattern       = [];
config.measurementPattern   = [];
for i = 1:2:numel(setup.coilGroups)
    currentPatternTemp          = createPattern(config.coilsActive, 'sequential', 'entityList', setup.coilGroups{i}(1:2:end), 'current', 1);
    measurementPatternTemp      = createPattern(config.sensorsActive, 'simultaneous', 'entityList', setup.sensorGroups{i}(1:2:end), 'times', size(currentPatternTemp, 1));
    config.currentPattern       = [config.currentPattern; currentPatternTemp];
    config.measurementPattern   = [config.measurementPattern; measurementPatternTemp];
end
if checkCompatibility(setup, config) % check compatibility with setup and save config
    saveConfig(sprintf('configs/%s/%s', config.info.name, config.info.variant), config, 'ff', true);
end

config.info.variant         = 'singleSequential4A';
config.currentPattern       = 4 .* config.currentPattern;
if checkCompatibility(setup, config) % check compatibility with setup and save config
    saveConfig(sprintf('configs/%s/%s', config.info.name, config.info.variant), config, 'ff', true);
end

% visualization has bug, since it shows coils and sensors, that are
% included in the coilsActive and sensorsActive field, BUT are not used by
% any pattern.
cla;visualizeMRX(setup, 'config', config, 'fid', fid, 'showCoils', 1, 'showSensors', 1, 'export', sprintf('configs/%s/visFull', config.info.name));
cla;visualizeMRX(setup, 'config', config, 'fid', fid, 'showCoils', 1, 'showSensors', 0, 'export', sprintf('configs/%s/visCoils', config.info.name));
cla;visualizeMRX(setup, 'config', config, 'fid', fid, 'showCoils', 0, 'showSensors', 1, 'export', sprintf('configs/%s/visSensors', config.info.name));




%%
config                      = [];
config.info.name            = 'all';
config.coilsActive          = unique([setup.coilGroups{:}]);
config.sensorsActive        = unique([setup.sensorGroups{:}]);

config.info.variant         = 'singleSequential';
config.currentPattern       = [];
config.measurementPattern   = [];
currentPatternTemp          = createPattern(config.coilsActive, 'sequential', 'current', 1);
measurementPatternTemp      = createPattern(config.sensorsActive, 'simultaneous', 'times', size(currentPatternTemp, 1));
config.currentPattern       = [config.currentPattern; currentPatternTemp];
config.measurementPattern   = [config.measurementPattern; measurementPatternTemp];
if checkCompatibility(setup, config) % check compatibility with setup and save config
    saveConfig(sprintf('configs/%s/%s', config.info.name, config.info.variant), config, 'ff', true);
end

cla;visualizeMRX(setup, 'config', config, 'fid', fid, 'showCoils', 1, 'showSensors', 1, 'export', sprintf('configs/%s/visFull', config.info.name));
cla;visualizeMRX(setup, 'config', config, 'fid', fid, 'showCoils', 1, 'showSensors', 0, 'export', sprintf('configs/%s/visCoils', config.info.name));
cla;visualizeMRX(setup, 'config', config, 'fid', fid, 'showCoils', 0, 'showSensors', 1, 'export', sprintf('configs/%s/visSensors', config.info.name));




%%
close(fid);

