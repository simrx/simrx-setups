
if(~isdeployed)
  cd([fileparts(which(mfilename)),'/..']);
end


%%
cla;
fid = figure('Visible', 'off');


%% create system

setup           = [];
setup.info.name = 'rotation2D';

setup.dim       = 2;

setup.roi.x     = [-.05, .05];
setup.roi.y     = [-.05, .05];
setup.roi.z     = [0, 0];

angleList               = (0:5:359.999999);
numAngles               = numel(angleList);
distance                = 0.075;
arrayWidth              = 0.17;
entitiesPerAngle    	= 15;
setup.sensors           = [];
setup.coils             = [];
for i = 1:numAngles
    alpha = (pi/180).*angleList(i);
    rotMat = [cos(alpha) -sin(alpha) 0; sin(alpha) cos(alpha) 0; 0 0 1];
    setup.sensors 	= [setup.sensors, createEntityArray((rotMat*[ distance, -arrayWidth/2, 0]')', (rotMat*[ distance, arrayWidth/2, 0]')', (rotMat*[-1, 0, 0]')', entitiesPerAngle)];
    setup.coils 	= [setup.coils,   createEntityArray((rotMat*[-distance, -arrayWidth/2, 0]')', (rotMat*[-distance, arrayWidth/2, 0]')', (rotMat*[ 1, 0, 0]')', entitiesPerAngle)];
end

setup.coilGroups    = mat2cell(reshape(1:numAngles*entitiesPerAngle, entitiesPerAngle, [])', ones(1,numAngles), entitiesPerAngle)';
setup.sensorGroups  = mat2cell(reshape(1:numAngles*entitiesPerAngle, entitiesPerAngle, [])', ones(1,numAngles), entitiesPerAngle)';


%% save setup variants

setup.coils = setCoilLoopFactor(setup.coils, 1);

setup.info.variant          = 'default';
saveSetup(setup.info.variant, setup, 'ff', true);
cla;visualizeMRX(setup, 'fid', fid, 'export', setup.info.variant);


%%
close(fid);

