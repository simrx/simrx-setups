
if(~isdeployed)
  cd([fileparts(which(mfilename)),'/..']);
end


%%
cla;
fid = figure('Visible', 'off');


%% create system

setup               = [];
setup.info.name     = 'basic2D';

setup.dim   = 2;

setup.roi.x = [-.05, .05];
setup.roi.y = [-.05, .05];
setup.roi.z = [   0,   0];


% coils

coilPosistionN = 60;
coilPosRadius = 0.075;
coilPosAngles = linspace(0, 2*pi, coilPosistionN+1)';
coilPosAngles(end) = [];

coilPosition    = coilPosRadius .* [cos(coilPosAngles),sin(coilPosAngles),zeros(size(coilPosAngles))];
coilNormal      = {...     
        [-cos(coilPosAngles+(0.00*pi)),-sin(coilPosAngles+(0.00*pi)),zeros(size(coilPosAngles))], ...  
        [-cos(coilPosAngles+(0.25*pi)),-sin(coilPosAngles+(0.25*pi)),zeros(size(coilPosAngles))], ...   
        [-cos(coilPosAngles-(0.25*pi)),-sin(coilPosAngles-(0.25*pi)),zeros(size(coilPosAngles))], ...   
        };
coilNormalN     = numel(coilNormal);

setup.coils     = [];
for i = 1:size(coilPosition,1)
    for j=1:coilNormalN
        setup.coils(coilNormalN*(i-1) + j).Position	= coilPosition(i,:);
        setup.coils(coilNormalN*(i-1) + j).Normal 	= coilNormal{j}(i,:);
    end
end

setup.coils     = [setup.coils, createEntityArray([-.055, -.05, 0], [-.055, .05, 0], [ 1, 0, 0], [1,21,1])];
setup.coils     = [setup.coils, createEntityArray([ .055, -.05, 0], [ .055, .05, 0], [-1, 0, 0], [1,21,1])];
setup.coils     = [setup.coils, createEntityArray([-.05, -.055, 0], [ .05,-.055, 0], [ 0, 1, 0], [21,1,1])];
setup.coils     = [setup.coils, createEntityArray([-.05,  .055, 0], [ .05, .055, 0], [ 0,-1, 0], [21,1,1])];

setup.coils     = [setup.coils, createEntityArray([-.055, -.05, 0], [-.055, .05, 0], [-0.5,-sqrt(3)*0.5, 0], [1,21,1])];
setup.coils     = [setup.coils, createEntityArray([ .055, -.05, 0], [ .055, .05, 0], [ 0.5, sqrt(3)*0.5, 0], [1,21,1])];
setup.coils     = [setup.coils, createEntityArray([-.05, -.055, 0], [ .05,-.055, 0], [ sqrt(3)*0.5,-0.5, 0], [21,1,1])];
setup.coils     = [setup.coils, createEntityArray([-.05,  .055, 0], [ .05, .055, 0], [-sqrt(3)*0.5, 0.5, 0], [21,1,1])];

setup.coils     = [setup.coils, createEntityArray([-.055, -.05, 0], [-.055, .05, 0], [-0.5, sqrt(3)*0.5, 0], [1,21,1])];
setup.coils     = [setup.coils, createEntityArray([ .055, -.05, 0], [ .055, .05, 0], [ 0.5,-sqrt(3)*0.5, 0], [1,21,1])];
setup.coils     = [setup.coils, createEntityArray([-.05, -.055, 0], [ .05,-.055, 0], [-sqrt(3)*0.5,-0.5, 0], [21,1,1])];
setup.coils     = [setup.coils, createEntityArray([-.05,  .055, 0], [ .05, .055, 0], [ sqrt(3)*0.5, 0.5, 0], [21,1,1])];

setup.coilGroups    = {1:3:180, 2:3:180, 3:3:180, 181:201, 265:285, 349:369, 202:222, 286:306, 370:390, 223:243, 307:327, 391:411, 244:264, 328:348, 412:432};;


% sensors

sensorPositionN = 60;
sensorPosRadius = 0.08;
sensorPosAngles = linspace(0, 2*pi, sensorPositionN+1)';
sensorPosAngles(end) = [];

sensorPosition    = sensorPosRadius .* [cos(sensorPosAngles),sin(sensorPosAngles),zeros(size(sensorPosAngles))];
sensorNormal      = {...     
        [-cos(sensorPosAngles+(0.00*pi)),-sin(sensorPosAngles+(0.00*pi)),zeros(size(sensorPosAngles))], ...   
        [-cos(sensorPosAngles+(0.25*pi)),-sin(sensorPosAngles+(0.25*pi)),zeros(size(sensorPosAngles))], ...   
        [-cos(sensorPosAngles-(0.25*pi)),-sin(sensorPosAngles-(0.25*pi)),zeros(size(sensorPosAngles))], ...
        };
sensorNormalN     = numel(sensorNormal);

setup.sensors = [];
for i = 1:size(sensorPosition,1)
    for j=1:sensorNormalN
        setup.sensors(sensorNormalN*(i-1) + j).Position	= sensorPosition(i,:);
        setup.sensors(sensorNormalN*(i-1) + j).Normal 	= sensorNormal{j}(i,:);
    end
end

setup.sensors     = [setup.sensors, createEntityArray([-.065, -.065, 0], [-.065, .065, 0], [ 1, 0, 0], [1,21,1], '-')];
setup.sensors     = [setup.sensors, createEntityArray([ .065, -.065, 0], [ .065, .065, 0], [-1, 0, 0], [1,21,1], '-')];
setup.sensors     = [setup.sensors, createEntityArray([-.065, -.065, 0], [ .065,-.065, 0], [ 0, 1, 0], [21,1,1], '-')];
setup.sensors     = [setup.sensors, createEntityArray([-.065,  .065, 0], [ .065, .065, 0], [ 0,-1, 0], [21,1,1], '-')];

setup.sensors     = [setup.sensors, createEntityArray([-.065, -.065, 0], [-.065, .065, 0], [-0.5,-sqrt(3)*0.5, 0], [1,21,1], '-')];
setup.sensors     = [setup.sensors, createEntityArray([ .065, -.065, 0], [ .065, .065, 0], [ 0.5, sqrt(3)*0.5, 0], [1,21,1], '-')];
setup.sensors     = [setup.sensors, createEntityArray([-.065, -.065, 0], [ .065,-.065, 0], [ sqrt(3)*0.5,-0.5, 0], [21,1,1], '-')];
setup.sensors     = [setup.sensors, createEntityArray([-.065,  .065, 0], [ .065, .065, 0], [-sqrt(3)*0.5, 0.5, 0], [21,1,1], '-')];

setup.sensors     = [setup.sensors, createEntityArray([-.065, -.065, 0], [-.065, .065, 0], [-0.5, sqrt(3)*0.5, 0], [1,21,1], '-')];
setup.sensors     = [setup.sensors, createEntityArray([ .065, -.065, 0], [ .065, .065, 0], [ 0.5,-sqrt(3)*0.5, 0], [1,21,1], '-')];
setup.sensors     = [setup.sensors, createEntityArray([-.065, -.065, 0], [ .065,-.065, 0], [-sqrt(3)*0.5,-0.5, 0], [21,1,1], '-')];
setup.sensors     = [setup.sensors, createEntityArray([-.065,  .065, 0], [ .065, .065, 0], [ sqrt(3)*0.5, 0.5, 0], [21,1,1], '-')];

setup.sensorGroups  = {1:3:180, 2:3:180, 3:3:180, 181:201, 265:285, 349:369, 202:222, 286:306, 370:390, 223:243, 307:327, 391:411, 244:264, 328:348, 412:432};


%% save setup variants
setup.info.variant          = 'default';
saveSetup(setup.info.variant, setup, 'ff', true);
cla;visualizeMRX(setup, 'fid', fid, 'export', setup.info.variant);


%%
close(fid);

