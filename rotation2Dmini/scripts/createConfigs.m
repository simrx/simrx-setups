
if(~isdeployed)
  cd([fileparts(which(mfilename)),'/..']);
end


%% load setup
setup = loadSetup('default');
fid = figure('Visible', 'off');


%%
configName                  = 'rotation';
config                      = [];
config.coilsActive          = unique([setup.coilGroups{:}]);
config.sensorsActive        = unique([setup.sensorGroups{:}]);

configVariant               = 'singleSequential';
config.currentPattern       = [];
config.measurementPattern   = [];
for i = 1:numel(setup.coilGroups)
    currentPatternTemp          = createPattern(config.coilsActive, 'sequential', 'entityList', setup.coilGroups{i}, 'current', 1);
    measurementPatternTemp      = createPattern(config.sensorsActive, 'simultaneous', 'entityList', setup.sensorGroups{i}, 'times', size(currentPatternTemp, 1));
    config.currentPattern       = [config.currentPattern; currentPatternTemp];
    config.measurementPattern   = [config.measurementPattern; measurementPatternTemp];
end
if checkCompatibility(setup, config) % check compatibility with setup and save config
    saveConfig(sprintf('configs/%s/%s', configName,configVariant), config, 'ff', true);
end

cla;visualizeMRX(setup, 'config', config, 'fid', fid, 'showCoils', 1, 'showSensors', 1, 'export', sprintf('configs/%s/visFull', configName));
cla;visualizeMRX(setup, 'config', config, 'fid', fid, 'showCoils', 1, 'showSensors', 0, 'export', sprintf('configs/%s/visCoils', configName));
cla;visualizeMRX(setup, 'config', config, 'fid', fid, 'showCoils', 0, 'showSensors', 1, 'export', sprintf('configs/%s/visSensors', configName));





%%
configName                  = 'all';
config                      = [];
config.coilsActive          = unique([setup.coilGroups{:}]);
config.sensorsActive        = unique([setup.sensorGroups{:}]);

configVariant               = 'singleSequential';
config.currentPattern       = [];
config.measurementPattern   = [];
currentPatternTemp          = createPattern(config.coilsActive, 'sequential', 'current', 1);
measurementPatternTemp      = createPattern(config.sensorsActive, 'simultaneous', 'times', size(currentPatternTemp, 1));
config.currentPattern       = [config.currentPattern; currentPatternTemp];
config.measurementPattern   = [config.measurementPattern; measurementPatternTemp];
if checkCompatibility(setup, config) % check compatibility with setup and save config
    saveConfig(sprintf('configs/%s/%s', configName,configVariant), config, 'ff', true);
end

cla;visualizeMRX(setup, 'config', config, 'fid', fid, 'showCoils', 1, 'showSensors', 1, 'export', sprintf('configs/%s/visFull', configName));
cla;visualizeMRX(setup, 'config', config, 'fid', fid, 'showCoils', 1, 'showSensors', 0, 'export', sprintf('configs/%s/visCoils', configName));
cla;visualizeMRX(setup, 'config', config, 'fid', fid, 'showCoils', 0, 'showSensors', 1, 'export', sprintf('configs/%s/visSensors', configName));




%%
close(fid);

