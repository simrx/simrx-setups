
if(~isdeployed)
  cd([fileparts(which(mfilename)),'/..']);
end


%%
cla;
fid = figure('Visible', 'off');


%% create system


setup               = [];
setup.info.name     = 'coilCalibration3D';

setup.dim       = 3;

setup.roi.x     = [-.05, .05];
setup.roi.y     = [-.05, .05];
setup.roi.z     = [-.05, .05];

setup.sensors 	= [];
setup.sensors 	= [setup.sensors, createEntityArray([-.045, -.045,  .065], [.045, .045,  .065], [ 0, 0,-1], [5,5,1])];
setup.sensors 	= [setup.sensors, createEntityArray([-.045, -.045, -.065], [.045, .045, -.065], [ 0, 0, 1], [5,5,1])];
setup.sensors 	= [setup.sensors, createEntityArray([-.045,  .065, -.045], [.045,  .065, .045], [ 0,-1, 0], [5,1,5])];
setup.sensors 	= [setup.sensors, createEntityArray([-.045, -.065, -.045], [.045, -.065, .045], [ 0, 1, 0], [5,1,5])];
setup.sensors 	= [setup.sensors, createEntityArray([ .065, -.045, -.045], [ .065, .045, .045], [-1, 0, 0], [1,5,5])];
setup.sensors 	= [setup.sensors, createEntityArray([-.065, -.045, -.045], [-.065, .045, .045], [ 1, 0, 0], [1,5,5])];

setup.sensorGroups  = {1:25, 26:50, 51:75, 76:100, 101:125, 126:150};


coilDistance        = 0.005;
setup.coils         = [];
for i = 1:10
    setup.coils         = [setup.coils, createEntityArray([setup.roi.x(1)-i*coilDistance,-.035,-.035], [setup.roi.x(1)-i*coilDistance, .035, .035], [ 1, 0, 0], [1 3 3])];
    setup.coils         = [setup.coils, createEntityArray([setup.roi.x(2)+i*coilDistance,-.035,-.035], [setup.roi.x(2)+i*coilDistance, .035, .035], [-1, 0, 0], [1 3 3])];
    setup.coils         = [setup.coils, createEntityArray([-.035,setup.roi.y(1)-i*coilDistance,-.035], [ .035,setup.roi.y(1)-i*coilDistance, .035], [0,  1, 0], [3 1 3])];
    setup.coils         = [setup.coils, createEntityArray([-.035,setup.roi.y(2)+i*coilDistance,-.035], [ .035,setup.roi.y(2)+i*coilDistance, .035], [0, -1, 0], [3 1 3])];
    setup.coils         = [setup.coils, createEntityArray([-.035,-.035,setup.roi.y(1)-i*coilDistance], [ .035, .035,setup.roi.y(1)-i*coilDistance], [0, 0,  1], [3 3 1])];
    setup.coils         = [setup.coils, createEntityArray([-.035,-.035,setup.roi.y(2)+i*coilDistance], [ .035, .035,setup.roi.y(2)+i*coilDistance], [0, 0, -1], [3 3 1])];
    
    setup.coilGroups{(i-1)*6 + 1} = ( ((i-1)*6*9) + 0*9 + 1 : ((i-1)*6*9) + 1*9 );
    setup.coilGroups{(i-1)*6 + 2} = ( ((i-1)*6*9) + 1*9 + 1 : ((i-1)*6*9) + 2*9 );
    setup.coilGroups{(i-1)*6 + 3} = ( ((i-1)*6*9) + 2*9 + 1 : ((i-1)*6*9) + 3*9 );
    setup.coilGroups{(i-1)*6 + 4} = ( ((i-1)*6*9) + 3*9 + 1 : ((i-1)*6*9) + 4*9 );
    setup.coilGroups{(i-1)*6 + 5} = ( ((i-1)*6*9) + 4*9 + 1 : ((i-1)*6*9) + 5*9 );
    setup.coilGroups{(i-1)*6 + 6} = ( ((i-1)*6*9) + 5*9 + 1 : ((i-1)*6*9) + 6*9 );
end
setup.coils         = [setup.coils, createEntityArray([-.055, 0, 0], [-.250, 0, 0], [ 1, 0, 0], 40)];
setup.coilGroups{61} = (541:580);



%%


setup.coils = setCoilLoopFactor(setup.coils, 1);
setup.info.variant          = 'idealizedUnscaled';
saveSetup(setup.info.variant, setup, 'ff', true);
cla;visualizeMRX(setup, 'fid', fid, 'export', setup.info.variant);
setup.info.variant          = 'idealized';
saveSetup(setup.info.variant, setup, 'ff', true);
cla;visualizeMRX(setup, 'fid', fid, 'export', setup.info.variant);


% coil_r0_015_w12_s1000_loop: the idealized coil is scaled so that it represents the
% coil_r0_015_w12_s1000_loop coil. The used factor is calculated using a calibration
% script.

coil_r0_015_w12_s1000_loop  = createCoilLoop(.015,12,1000,'loop');
coil_r0_015_w12_s1000_loopF = 0.0084307187;

setup.coils             = setCoilLoopFactor(setup.coils, 1);
setup.coils             = parseCoils(setup.coils,coil_r0_015_w12_s1000_loop);
setup.info.variant          = 'coil_r0.015_w12_s1000_loop';
saveSetup(setup.info.variant, setup, 'ff', true);
cla;visualizeMRX(setup, 'fid', fid, 'export', setup.info.variant);

setup.coils             = setCoilLoopFactor(setup.coils, coil_r0_015_w12_s1000_loopF);
setup.coils             = parseCoils(setup.coils,[]);
setup.info.variant      = 'coil_r0.015_w12_s1000_loop_idealized';
saveSetup(setup.info.variant, setup, 'ff', true);
cla;visualizeMRX(setup, 'fid', fid, 'export', setup.info.variant);

setup.info.variant          = 'default';
saveSetup(setup.info.variant, setup, 'ff', true);
cla;visualizeMRX(setup, 'fid', fid, 'export', setup.info.variant);



%%
close(fid);

