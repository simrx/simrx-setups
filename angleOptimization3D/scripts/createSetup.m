
if(~isdeployed)
  cd([fileparts(which(mfilename)),'/..']);
end


%%
cla;
fid = figure('Visible', 'off');


%% create system

setup           = [];
setup.info.name = 'angleOptimization3D';

setup.dim       = 3;

setup.roi.x     = [-.05, .05];
setup.roi.y     = [-.05, .05];
setup.roi.z     = [-.05, .05];

angleList               = (0:5:359.999999);
numAngles               = numel(angleList);
coilDistance            = 0.075;
sensorDistance          = 0.08;
arrayWidth              = 0.10;
entitiesPerAngle    	= 9;
setup.sensors           = [];
setup.coils             = [];
for i = 1:numAngles
    alpha = (pi/180).*angleList(i);
    rotMat = [cos(alpha) -sin(alpha) 0 ; sin(alpha) cos(alpha) 0; 0 0 1];
    setup.sensors   = [setup.sensors,   createEntityArray((rotMat*[sensorDistance, 0, -arrayWidth/2]')', (rotMat*[sensorDistance, 0, arrayWidth/2]')', (rotMat*[-1, 0, 0]')', entitiesPerAngle)];
    setup.coils 	= [setup.coils,   createEntityArray((rotMat*[coilDistance, 0, -arrayWidth/2]')', (rotMat*[coilDistance, 0, arrayWidth/2]')', (rotMat*[-1, 0, 0]')', entitiesPerAngle)];
end

setup.coilGroups    = [mat2cell(reshape(1:numAngles*entitiesPerAngle, [], numAngles), ones(1,entitiesPerAngle), numAngles)' , ...
    mat2cell(reshape(1:numAngles*entitiesPerAngle, entitiesPerAngle, [])', ones(1,numAngles), entitiesPerAngle)'];
setup.sensorGroups  = [mat2cell(reshape(1:numAngles*entitiesPerAngle, [], numAngles), ones(1,entitiesPerAngle), numAngles)' , ...
    mat2cell(reshape(1:numAngles*entitiesPerAngle, entitiesPerAngle, [])', ones(1,numAngles), entitiesPerAngle)'];


%% save setup variants

setup.coils = setCoilLoopFactor(setup.coils, 1);

setup.info.variant          = 'default';
saveSetup(setup.info.variant, setup, 'ff', true);
cla;visualizeMRX(setup, 'fid', fid, 'export', setup.info.variant);


%%
close(fid);

