%   README.m is used to run all scripts related to this setup


%   Author:     Lea Föcke
%   E-Mail:     lea.foecke@fau.de
%   Institute:  FAU Erlangen-Nürnberg
%   Date:       26-May-2021


if(~isdeployed)
  cd(fileparts(which(mfilename)));
end


%% load SiMRX toolbox
run('../../SiMRX/initSiMRX.m'); % load SiMRX path relative to this example script

setupName = 'angleOptimization3D';


%%
decoratedBox(['Cleaup ''',setupName,'''']);

if isfolder('configs');         rmdir('configs','s'); end
if isfolder('measurements');    rmdir('measurements','s'); end

if isfolder('raw')
    str = input('delete raw folder? Y/N [N]: ','s');
    if isempty(str); str = 'N'; end
    if any(strcmp(str,{'Y','y','yes'}))
        theFiles = dir('raw'); theFiles(1:2) = [];
        theFiles = fullfile('raw', {theFiles.name});
        for k = 1: length(theFiles)
            if isfolder(theFiles{k})
                rmdir(theFiles{k},'s');
            else
                delete(theFiles{k});
            end
        end
    end
end

theFiles = [dir('*.mrxsetup'); dir('*.png')];
for k = 1 : length(theFiles)
  delete(theFiles(k).name);
end

decoratedBox(['Cleaup ''',setupName,''' done']);


%%

decoratedBox(['Run scripts for setup ''',setupName,'''']);

% First create the setup file 'default.mrxsetup'
run('scripts/createSetup.m');

% Second create a config file that is suitable for 'default.mrxsetup'
run('scripts/createConfigs.m');

decoratedBox(['setup ''',setupName,''' done']);


