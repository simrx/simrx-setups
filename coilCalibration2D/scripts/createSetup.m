
if(~isdeployed)
  cd([fileparts(which(mfilename)),'/..']);
end


%%
cla;
fid = figure('Visible', 'off');


%% create system

setup               = [];
setup.info.name     = 'coilCalibration2D';

setup.dim       = 2;

setup.roi.x     = [-.05, .05];
setup.roi.y     = [-.05, .05];
setup.roi.z     = [0, 0];

setup.sensors 	= [];
setup.sensors 	= [setup.sensors, createEntityArray([-.05,  .055, 0], [.05,  .055, 0], [ 0,-1, 0], [10,1,1])];
setup.sensors 	= [setup.sensors, createEntityArray([-.05, -.055, 0], [.05, -.055, 0], [ 0, 1, 0], [10,1,1])];
setup.sensors 	= [setup.sensors, createEntityArray([ .055, -.05, 0], [ .055, .05, 0], [-1, 0, 0], [1,10,1])];
setup.sensors 	= [setup.sensors, createEntityArray([-.055, -.05, 0], [-.055, .05, 0], [ 1, 0, 0], [1,10,1])];

setup.sensorGroups  = {1:10, 11:20, 21:30, 31:40};


coilDistance        = 0.005;
setup.coils         = [];
setup.info.coilPerLayer  = 10;
setup.info.numLayer      = 20;
for i = 1:setup.info.numLayer
    setup.coils         = [setup.coils, createEntityArray([setup.roi.x(1)-i*coilDistance,-.035,-.035], [setup.roi.x(1)-i*coilDistance, .035, .035], [ 1, 0, 0], [1 setup.info.coilPerLayer 1])];
    setup.coilGroups{i} = ((i-1)*setup.info.coilPerLayer + 1 : i*setup.info.coilPerLayer);
end



%%

setup.coils             = setCoilLoopFactor(setup.coils, 1);
setup.info.variant      = 'default';
saveSetup(setup.info.variant, setup, 'ff', true);
cla;visualizeMRX(setup, 'fid', fid, 'export', setup.info.variant);


%%
close(fid);

