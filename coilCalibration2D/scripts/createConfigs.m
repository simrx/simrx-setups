
if(~isdeployed)
  cd([fileparts(which(mfilename)),'/..']);
end


%% load setup
setup = loadSetup('default');
fid = figure('Visible', 'off');


%%
for i=1:setup.info.numLayer
    config                      = [];
    config.info.name            = sprintf('dist%02imm', i*5);
    config.coilsActive          = unique([setup.coilGroups{i}]);
    config.sensorsActive        = unique([setup.sensorGroups{:}]);

    config.info.variant         = 'singleSequential';
    config.currentPattern       = [];
    config.measurementPattern   = [];
    currentPatternTemp          = createPattern(config.coilsActive, 'sequential', 'current', 1);
    measurementPatternTemp      = createPattern(config.sensorsActive, 'simultaneous', 'times', size(currentPatternTemp, 1));
    config.currentPattern       = [config.currentPattern; currentPatternTemp];
    config.measurementPattern   = [config.measurementPattern; measurementPatternTemp];
    if checkCompatibility(setup, config) % check compatibility with setup and save config
        saveConfig(sprintf('configs/%s/%s', config.info.name, config.info.variant), config, 'ff', true);
    end

    config.info.variant         = 'simultaneous';
    config.currentPattern       = [];
    config.measurementPattern   = [];
    currentPatternTemp          = createPattern(config.coilsActive, 'simultaneous', 'current', 1);
    measurementPatternTemp      = createPattern(config.sensorsActive, 'simultaneous', 'times', size(currentPatternTemp, 1));
    config.currentPattern       = [config.currentPattern; currentPatternTemp];
    config.measurementPattern   = [config.measurementPattern; measurementPatternTemp];
    if checkCompatibility(setup, config) % check compatibility with setup and save config
        saveConfig(sprintf('configs/%s/%s', config.info.name, config.info.variant), config, 'ff', true);
    end
    
    cla;visualizeMRX(setup, 'config', config, 'fid', fid, 'showCoils', 1, 'showSensors', 1, 'export', sprintf('configs/%s/visFull', config.info.name));
    cla;visualizeMRX(setup, 'config', config, 'fid', fid, 'showCoils', 1, 'showSensors', 0, 'export', sprintf('configs/%s/visCoils', config.info.name));
    cla;visualizeMRX(setup, 'config', config, 'fid', fid, 'showCoils', 0, 'showSensors', 1, 'export', sprintf('configs/%s/visSensors', config.info.name));
    
end


%%
config                      = [];
config.info.name            = 'all';
config.coilsActive          = unique([setup.coilGroups{:}]);
config.sensorsActive        = unique([setup.sensorGroups{:}]);

config.info.variant         = 'singleSequential';
config.currentPattern       = [];
config.measurementPattern   = [];
currentPatternTemp          = createPattern(config.coilsActive, 'sequential', 'current', 1);
measurementPatternTemp      = createPattern(config.sensorsActive, 'simultaneous', 'times', size(currentPatternTemp, 1));
config.currentPattern       = [config.currentPattern; currentPatternTemp];
config.measurementPattern   = [config.measurementPattern; measurementPatternTemp];
if checkCompatibility(setup, config) % check compatibility with setup and save config
    saveConfig(sprintf('configs/%s/%s', config.info.name, config.info.variant), config, 'ff', true);
end

cla;visualizeMRX(setup, 'config', config, 'fid', fid, 'showCoils', 1, 'showSensors', 1, 'export', sprintf('configs/%s/visFull', config.info.name));
cla;visualizeMRX(setup, 'config', config, 'fid', fid, 'showCoils', 1, 'showSensors', 0, 'export', sprintf('configs/%s/visCoils', config.info.name));
cla;visualizeMRX(setup, 'config', config, 'fid', fid, 'showCoils', 0, 'showSensors', 1, 'export', sprintf('configs/%s/visSensors', config.info.name));

 
%%
close(fid);

